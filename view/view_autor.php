<!-- <div id="incuir_autor" class="container">
	<form method="POST" action="controler/incluir_autor.php" id="form_incluir_autor">
		<input type="text" name="nome_autor" placeholder="Informe o nome do autor" class="campo">
		<input type="submit" name="cadastrar" class="botao">
	</form>
</div> -->

<?php 
	$met = new Metodos();

	$sql = "select * from autor ORDER BY nome_autor";
	$dados=$met->consultarAutor();

?>
<div class="container" id="lista_autores">
	<a class="btn btn-primary" href="?pag=incluir_autor" border: none;">Inserir Novo Autor</a><p>


	<table class="table table-hover table-striped" id="autores">
		<thead>
			<tr>
				<th>Autor(a)</th>
				<th>Editar</th>
				<th>Deletar</th>
			</tr>
		</thead>

		<tbody>
		<?php 
			foreach ($dados as $key) {?>
				<tr>
					<input type='hidden' name='cod_autor' value="<?php echo $key['cod_autor']; ?>">
					<td><?php echo $key['nome_autor']; ?></td>
					<td><a href="?pag=editar_autor&cod_autor=<?php echo $key['cod_autor']; ?>" ><span style="color: #F94804; margin-left: 47%;"><i class="fas fa-edit"></i></span></a></td>
					<td><a href="?pag=remover_autor&cod_autor=<?php echo $key['cod_autor']; ?>" ><span style="color: #F94804; margin-left: 47%;"><i class="fas fa-trash-alt"></i></span></a></td>
				</tr>	
			<?php } ?>
		</tbody>

	</table>
</div>