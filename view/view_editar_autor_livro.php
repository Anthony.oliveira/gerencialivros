<?php 
	
	$met = new Metodos();
	$autor = new Autor();
	
	$livro = new Livro();
	$livro->setISBN($_GET['isbn']);

	//$sql = "select * from autor";
	$autores=$met->consultarAutor();
	//$sql = " select autor.cod_autor, autor.nome_autor from autor, autor_livro, livro where livro.isbn='".$isbn."' and autor_livro.fk_livro = livro.isbn and fk_autor=cod_autor;";
	
	$dados=$met->consultarAutorLivro($livro);

	

?>
<div id="incluir_livro" class="container">
	<form method="POST" action="controler/cadastrar_autor_livro.php?isbn=<?php echo $livro->getISBN(); ?>">
		<label for="exampleFormControlSelect1">Escolha os autores</label>
		
		<div class="form-group">
			<select name="aut" class="form-control" id="exampleFormControlSelect1">
			<option value="0">Escolha</option>
			<?php foreach ($autores as $key) {?>
				<option value="<?php echo $key['cod_autor'];?>"><?php echo $key['nome_autor'];?></option>
			<?php } ?>
			</select>
		</div>


		<input class="btn btn-primary" type="submit" value="Adicionar">

	</form>
</div>


<div class="container" id="lista_autores">


	<table class="table table-hover table-striped" id="cursos">
		<thead>
			<tr>
				<th>Autor(a)</th>
				<th>Excluir</th>
			</tr>
		</thead>

		<tbody>
		<?php 
			foreach ($dados as $key) {?>
				<tr>
					<input type='hidden' name='cod_autor' value="<?php echo $key['cod_autor']; ?>">
					<td><?php echo $key['nome_autor']; ?></td>
					<td><a href="?pag=remover_autor_livro&cod_autor=<?php echo $key['cod_autor']; ?>&isbn=<?php echo $livro->getISBN() ?>" ><span style="color: #F94804;"><i class="fas fa-trash-alt"></i></span></a></td>
				</tr>	
			<?php } ?>
		</tbody>

	</table>
	<form method="POST" action="?pag=livro">
		<input class="btn btn-success" type="submit" value="Concluir">
	</form>
</div>