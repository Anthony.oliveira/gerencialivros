<?php 
	$met = new Metodos();
	$livro = new Livro();

	$dados=$met->consultarLivro();


?>
<div class="container" id="lista_livros">
	<a class="btn btn-primary" href="?pag=incluir_livro" border: none;">Inserir Novo Livro</a><p>


	<table class="table table-hover table-striped" id="livros">
		<thead>
			<tr>
				<th>ISBN</th>
				<th>Título</th>
				<th>Ano de Publicação</th>
				<th>Preço</th>
				<th>Páginas</th>
				<th>Editora</th>
				<th>Autor(es)</th>
				<th>Editar</th>
				<th>Excluir</th>
			</tr>
		</thead>

		<tbody>
		<?php 
			foreach ($dados as $key) {?>
				<tr>
					<td><?php echo $key['isbn']; ?></td>
					<td><?php echo $key['titulo']; ?></td>
					<td><?php echo $key['data_publicacao']; ?></td>
					<td><?php echo $key['preco']; ?></td>
					<td><?php echo $key['paginas']; ?></td>
					<td><?php echo $key['editora']; ?></td>
					<?php 
					$livro->setISBN($key['isbn']);
					
					$get_autor_livro = $met->consultarAutorLivro($livro);
					$autores="";
					foreach ($get_autor_livro as $ind) {
						$autores = $autores.$ind['nome_autor']." / ";
					} ?>
					<td><?php echo $autores; ?></td>
					<td><a href="?pag=editar_livro&isbn=<?php echo $key['isbn']; ?>" ><span style="color: #F94804; margin-left: 47%;"><i class="fas fa-edit"></i></span></a></td>
					<td><a href="?pag=remover_livro&isbn=<?php echo $key['isbn']; ?>" ><span style="color: #F94804; margin-left: 47%;"><i class="fas fa-trash-alt"></i></span></a></td>
				</tr>	
			<?php } ?>
		</tbody>

	</table>
</div>