<?php 
	$met = new Metodos();
	$autor = new Autor();
	
	

	if (isset($_POST['nome_autor'])) 
	{
		$autor->setNome_autor($_POST['nome_autor']);
		$dados=$met->pesquisa($autor);
	}else{
$autor->setNome_autor($_GET['nome_autor']);
		$dados=$met->pesquisa($autor);
	}


?>

<div id="incluir_autor" class="container">
<div class="alert alert-secondary" role="alert">  PESQUISAR </div>
	<form method="POST" action="?pag=pesquisa&nome_autor=" id="form_incluir_autor">
		<input type="text" name="nome_autor" placeholder="Informe o nome do autor para pesquisar" class="campo">
		<input class="btn btn-primary" type="submit" name="cadastrar" value="Pesquisar">
	</form>
</div>

<div class="container" id="lista_livros">



	<table class="table table-hover table-striped" id="pesquisa">
		<thead>
			<tr>
				<th>ISBN</th>
				<th>Autor</th>
				<th>Título</th>
				<th>Detalhes</th>
			</tr>
		</thead>

		<tbody>
		<?php 
			foreach ($dados as $key) {?>
				<tr>
					<td data-mask="000-00-0000-000-0"><?php echo $key['isbn']; ?></td>
					<td><?php echo $key['nome_autor']; ?></td>
					<td><?php echo $key['titulo']; ?></td>
					<td><a href="?pag=detalhe_pesquisa&isbn=<?php echo $key['isbn']; ?>"><i class="fas fa-search" style="font-size: 30px; margin-left: 47%;"></i></a></td>
				</tr>	
			<?php } ?>
		</tbody>

	</table>
</div>