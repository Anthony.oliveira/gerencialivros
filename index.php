<?php 

# Base de dados
include 'config/db.php';
include 'config/metodos.php';
include 'model/autor.php';
include 'model/livro.php';


# Cabeçalho
include "header.php";

# Páginas


if (isset($_GET['pag'])) 
{
	$pagina = $_GET['pag'];
}else {
		$pagina = '#';
	}	
	
	switch ($pagina) {
  	
  	case '#': include "view/view_home.php"; break;
  	case 'autor': include "view/view_autor.php"; break;
  	case 'incluir_autor': include "view/view_incluir_autor.php"; break;
	case 'remover_autor': include "controler/remover_autor.php"; break;
	case 'editar_autor': include "view/view_editar_autor.php"; break;
  	case 'livro': include "view/view_livro.php"; break;
  	case 'incluir_livro': include "view/view_incluir_livro.php"; break;
	case 'remover_livro': include "controler/remover_livro.php"; break;
	case 'editar_livro': include "view/view_editar_livro.php"; break;
	case 'cadastrar_autor_livro': include "view/view_cadastrar_autor_livro.php"; break;
	case 'editar_autor_livro': include "view/view_editar_autor_livro.php"; break;
	case 'remover_autor_livro': include "controler/remover_autor_livro.php"; break;
	case 'pesquisa': include "view/view_pesquisa.php"; break;
	case 'detalhe_pesquisa': include "view/view_detalhes_pesquisa.php"; break;
  }

?>



<?php 
# Rodapé
include "footer.php"; ?>