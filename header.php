<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Gerenciamento de Livros</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<script src="https://kit.fontawesome.com/451c3eb774.js"></script>
</head>
<body>

<header>
	<div id="topo">
		<div id="menu">
			<nav>
				<a href="index.php">Home</a>
				<a href="index.php?pag=autor">Autores</a>
				<a href="index.php?pag=livro">Livros</a>
				<a href="index.php?pag=pesquisa&nome_autor=''">Pesquisar Livros</a>
			</nav>
		</div>
	</div>
</header>


