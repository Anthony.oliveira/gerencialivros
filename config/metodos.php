<?php 

class Metodos
{

	public function consultarAutor()
	{
		$c = new db();
		$conn = $c->Conectar();
		$sql = "select * from autor ORDER BY nome_autor";
		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function consultarLivro()
	{
		$c = new db();
		$conn = $c->Conectar();
		$sql = "select * from livro ORDER BY titulo";
		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function consultarAutorCodigo($Autor)
	{
		$c = new db();
		$conn = $c->Conectar();
		$sql = "select * from autor where cod_autor=".$Autor->getCodigo_autor();
		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function incluirAutor($Autor)
	{
		$objAutor = new Autor();
		$objAutor->setNome_autor($Autor->getNome_autor());
		$sql = "insert into autor (nome_autor) values ('".$objAutor->getNome_autor()."');";

		$c = new db();
		$conn = $c->Conectar();	
		mysqli_query($conn, $sql) or die().mysql_error();
	}

	public function removerAutor($Autor)
	{
		$objAutor = new Autor();
		$objAutor->setCodigo_autor($Autor->getCodigo_autor());
		$sql = "delete from autor where cod_autor=".$objAutor->getCodigo_autor();
		$c = new db();
		$conn = $c->Conectar();

		mysqli_query($conn, $sql);

	}

	public function edtarAutor($Autor)
	{
		$objAutor = new Autor();
		$objAutor->setCodigo_autor($Autor->getCodigo_autor());
		$objAutor->setNome_autor($Autor->getNome_autor());
		$sql = "update autor set nome_autor ='".$objAutor->getNome_autor()."' where cod_autor=".$objAutor->getCodigo_autor();
		$c = new db();
		$conn = $c->Conectar();	
		mysqli_query($conn, $sql);

	}

	public function edtarLivro($Livro)
	{
		$c = new db();
		$conn = $c->Conectar();
		$objLivro = new Livro();
		$objLivro->setISBN($Livro->getISBN());
		$objLivro->setTitulo($Livro->getTitulo());
		$objLivro->setData_publicacao($Livro->getData_publicacao());
		$objLivro->setEditora($Livro->getEditora());
		$objLivro->setPaginas($Livro->getPaginas());
		$objLivro->setPreco($Livro->getPreco());

		$sql = "update livro set isbn='".$objLivro->getISBN()."', titulo='".$objLivro->getTitulo()."', data_publicacao ='".$objLivro->getData_publicacao()."', editora = '".$objLivro->getEditora()."', paginas = ".$objLivro->getPaginas().", preco = ".$objLivro->getPreco()." where isbn='".$objLivro->getISBN()."'";
		mysqli_query($conn, $sql);
		//echo $sql;

	}

	public function consultarLivroISBN($sql)
	{
		$c = new db();
		$conn = $c->Conectar();

		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function pesquisa($Autor)
	{
		$c = new db();
		$conn = $c->Conectar();
		$objAutor = new Autor();
		$objAutor->setNome_autor($Autor->getNome_autor());
		$sql = "select livro.isbn, autor.nome_autor, livro.titulo from livro, autor_livro, autor where autor.cod_autor=autor_livro.fk_autor and autor_livro.fk_livro=livro.isbn and autor.nome_autor like'%".$objAutor->getNome_autor()."%'";

		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function removerLivro($Livro)
	{
		$c = new db();
		$conn = $c->Conectar();
		$objLivro = new Livro();
		$objLivro->setISBN($Livro->getISBN());
		$sql = "delete from livro where isbn='".$objLivro->getISBN()."'";

		mysqli_query($conn, $sql);

	}

	public function consultarAutorLivro($Livro)
	{
		$c = new db();
		$conn = $c->Conectar();
		$objLivro = new Livro();
		$objLivro->setISBN($Livro->getISBN());
		$sql = "select autor.cod_autor, autor.nome_autor from autor, autor_livro, livro where fk_autor=cod_autor and isbn='".$objLivro->getISBN()."' and fk_livro=isbn;";

		$result = mysqli_query($conn, $sql) ;

		return mysqli_fetch_all($result,MYSQLI_ASSOC);
	}

	public function incluirLivro($Livro)
	{
		$c = new db();
		$conn = $c->Conectar();
		$objLivro = new Livro();
		$objLivro->setISBN($Livro->getISBN());
		$objLivro->setTitulo($Livro->getTitulo());
		$objLivro->setData_publicacao($Livro->getData_publicacao());
		$objLivro->setEditora($Livro->getEditora());
		$objLivro->setPaginas($Livro->getPaginas());
		$objLivro->setPreco($Livro->getPreco());

		$sql = "insert into livro (isbn, titulo, data_publicacao, preco, paginas, editora) values ('".$objLivro->getISBN()."', '".$objLivro->getTitulo()."', '".$objLivro->getData_publicacao()."', ".$objLivro->getPreco().", ".$objLivro->getPaginas().", '".$objLivro->getEditora()."');";

		$result = mysqli_query($conn, $sql);
	}

	public function incluirAutorLivro($Autor, $Livro)
	{
		$objAutor = new Autor();
		$objLivro = new Livro();
		$objAutor->setCodigo_autor($Autor->getCodigo_autor());
		$objLivro->setISBN($Livro->getISBN());
		$sql = "insert into autor_livro (fk_livro, fk_autor) values ('".$objLivro->getISBN()."', ".$objAutor->getCodigo_autor().");";
		echo $sql;
		$c = new db();
		$conn = $c->Conectar();	
		mysqli_query($conn, $sql);
	}

	public function removerAutorLivro($Autor, $Livro)
	{
		$objAutor = new Autor();
		$objLivro = new Livro();
		$objAutor->setCodigo_autor($Autor->getCodigo_autor());
		$objLivro->setISBN($Livro->getISBN());
		$sql = "delete from autor_livro where fk_autor=".$objAutor->getCodigo_autor()." and fk_livro='".$objLivro->getISBN()."'";

		$c = new db();
		$conn = $c->Conectar();

		mysqli_query($conn, $sql);

	}

}