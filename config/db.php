<?php 

class db
{
	private $servidor;
	private $usuario;
	private $senha;
	private $db;

	public function __construct()
	{
		$this->servidor = "localhost";
		$this->usuario = "root";
		$this->senha = "";
		$this->db = "gerencia_livros";
	}

	public function Conectar()
	{
		$conexao = mysqli_connect($this->servidor, $this->usuario, $this->senha, $this->db);
		return $conexao;
	}

}

