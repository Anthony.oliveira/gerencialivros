<?php 
class Autor
{
	private $cod_autor;
	private $nome_autor;

	public function setCodigo_autor($valor)
	{
		$this->cod_autor = $valor;
	}

	public function setNome_autor($valor)
	{
		$this->nome_autor = $valor;
	}

	public function getCodigo_autor()
	{
		return $this->cod_autor;
	}

	public function getNome_autor()
	{
		return $this->nome_autor;
	}
}