<?php 

class Livro {
    private $ISBN;
    private $titulo;
    private $data_publicacao;
    private $editora;
    private $paginas;
    private $preco;

    public function setISBN($valor)
	{
		$this->ISBN = $valor;
	}

	public function setTitulo($valor)
	{
		$this->titulo = $valor;
	}

	public function setData_publicacao($valor)
	{
		$this->data_publicacao = $valor;
	}

	public function setEditora($valor)
	{
		$this->editora = $valor;
	}

	public function setPaginas($valor)
	{
		$this->paginas = $valor;
	}

	public function setPreco($valor)
	{
		$this->preco = $valor;
	}


	public function getISBN()
	{
		return $this->ISBN;
	}

	public function getTitulo()
	{
		return $this->titulo;
	}

	public function getData_publicacao()
	{
		return $this->data_publicacao;
	}

	public function getEditora()
	{
		return $this->editora;
	}

	public function getPaginas()
	{
		return $this->paginas;
	}

	public function getPreco()
	{
		return $this->preco;
	}

    
}


 ?>