  </div>

  <footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="js/jquery.mask.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <script src="js/personalizado.js"></script>

    <script>
    $(document).ready( function () {
        $('#autores').DataTable();
        $('#livros').DataTable();
        $('#pesquisa').DataTable();
    } );
     
     $(document).ready(function(){
		  $('.date').mask('00/00/0000');
		  $('.isbn').mask('000-00-0000-000-0');
		  $('.isbn_unmask').unmask();
		  $('.time').mask('00:00:00');
		  $('.date_time').mask('00/00/0000 00:00:00');
		  $('.cep').mask('00000-000');
		  $('.phone').mask('0000-0000');
		  $('.phone_with_ddd').mask('(00) 0000-0000');
		  $('.phone_us').mask('(000) 000-0000');
		  $('.mixed').mask('AAA 000-S0S');
		  $('.cpf').mask('000.000.000-00', {reverse: true});
		  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
		  $('.money').mask('000.000.000.000.000,00', {reverse: true});
		  $('.money2').mask("#.##0,00", {reverse: true});
		  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
		    translation: {
		      'Z': {
		        pattern: /[0-9]/, optional: true
		      }
		    }
		  });
		  $('.ip_address').mask('099.099.099.099');
		  $('.percent').mask('##0,00%', {reverse: true});
		  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
		  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
		  $('.fallback').mask("00r00r0000", {
		      translation: {
		        'r': {
		          pattern: /[\/]/,
		          fallback: '/'
		        },
		        placeholder: "__/__/____"
		      }
		    });
		  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
	});

  </script>

  </footer>
</body>
</html>