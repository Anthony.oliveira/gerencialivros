<?php 
	include '../config/db.php';
	include '../config/metodos.php';
	include '../model/livro.php';
	
	$met = new Metodos();
	$livro = new Livro();

	$livro->setISBN($_POST["isbn"]);
	$livro->setTitulo($_POST["titulo"]);
	$livro->setData_publicacao($_POST["data_publicacao"]);
	$livro->setEditora($_POST["editora"]);
	$livro->setPaginas($_POST["paginas"]);
	$livro->setPreco($_POST["preco"]);
	
	$met->edtarLivro($livro);
	echo $livro->getISBN();
	header("Location: ../?pag=editar_autor_livro&isbn=".$livro->getISBN()."&cod=0");

?>
